import Ui = GoogleAppsScript.Base.Ui;

const onOpen = () => {
    const ui: Ui = SpreadsheetApp.getUi();
    ui.createMenu('Accounting')
        .addSubMenu(
            ui.createMenu('Import')
                .addItem('Import data', 'doImport')
        )
        .addToUi();
}
