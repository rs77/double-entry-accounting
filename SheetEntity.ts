import Sheet = GoogleAppsScript.Spreadsheet.Sheet;
import Spreadsheet = GoogleAppsScript.Spreadsheet.Spreadsheet;


export const getEntityIdList = () => {
    const ss: Spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    const sheetName: string = "Entity";
    const sht: Sheet = ss.getSheetByName(sheetName);
    const data: any[][] = sht.getDataRange().getValues();
    const header = ss.getRangeByName("EntityIDs").getValues();
    const fr = sht.getFrozenRows();
    return data.reduce((accum: string[], row: any, idx: number): string[] => {
        if (idx >= fr) {
            const entityId = row[0];
            accum.push(entityId);
        }
        return accum;
    }, []);
}