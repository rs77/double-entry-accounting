

export const getColumnarObjectFromKeys = (keys: string[], defaultValue: any) => {
    return keys.reduce((accum: object, key: string) => {
        accum[key] = JSON.parse(JSON.stringify(defaultValue));
        return accum;
    }, {});
}

export const copyObjectContentsIntoObjectAsRow = (sourceObject: object, insertObject: object): void => {
    for (let key in insertObject) {
        const val = insertObject[key];
        // even if `val` is an empty string it still must be added
        if (sourceObject.hasOwnProperty(key)) sourceObject[key].push([val]);
    }
}