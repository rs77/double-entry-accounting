import Spreadsheet = GoogleAppsScript.Spreadsheet.Spreadsheet;
import {
  copyObjectContentsIntoObjectAsRow,
  getColumnarObjectFromKeys,
} from "./Utilities";
import Ui = GoogleAppsScript.Base.Ui;
import Button = GoogleAppsScript.Base.Button;
import {getAccountIdList} from "./SheetCategories";
import Sheet = GoogleAppsScript.Spreadsheet.Sheet;
import {getEntityIdList} from "./SheetEntity";
import Range = GoogleAppsScript.Spreadsheet.Range;

const doImport = () => {
  const ui: Ui = SpreadsheetApp.getUi();
  const ss: Spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  const shtImport: Sheet = ss.getSheetByName("Import");
  ss.setActiveSheet(shtImport);
  SpreadsheetApp.flush();
  const proceed: Button = ui.alert( "Import Data", "Do you want to import the data from this sheet?", ui.ButtonSet.YES_NO);
  // check user wants to import data
  if (proceed == ui.Button.NO) return;
  // check warnings first to see if you can proceed
  const errorReport: string[][] = [];
  // Get all other relevant Settings from the Import sheet
  const acctID: string = ss.getRangeByName("ImportAccountID").getValue();
  // opening balance details
  const openBal: number = ss.getRangeByName("ImportOpenBalance").getValue();
  const openBalDate: Date = ss.getRangeByName("ImportOpenBalanceDate").getValue();
  const openBalAccountID: string = ss.getRangeByName("ImportOpenBalanceAcctID").getValue();
  const debitHandling: string = ss.getRangeByName("ImportDebitAmountHandling").getValue();
  const mainLineFieldName: string = ss.getRangeByName("setImportMainLineField").getValue();
  const detailTransFieldName: string = ss.getRangeByName("setTransactionDetailField").getValue();
  const debitAmountTransFieldName: string = ss.getRangeByName("setTransactionDebitAmountField").getValue();
  const creditAmountTransFieldName: string = ss.getRangeByName("setTransactionCreditAmountField").getValue();
  const dateTransFieldName: string = ss.getRangeByName("setTransactionDateField").getValue();
  const acctIdTransFieldName: string = ss.getRangeByName("setTransactionAccountIDField").getValue();
  // The Import Field row only contains one row of data
  const importFieldMap: string[] = ss.getRangeByName("ImportFieldMap").getValues()[0];
  const hasDateField: boolean = !~importFieldMap.indexOf(dateTransFieldName);
  const hasAcctIdField: boolean = !~importFieldMap.indexOf(acctIdTransFieldName);
  // Check if Import Fields contain the required Date & Account fields
  if (hasDateField || hasAcctIdField) {
    const alertFields: string = hasDateField && hasAcctIdField ? "Date and Account ID" :
        hasDateField ? "Date" :
            hasAcctIdField ? "Account ID" : "";
    ui.alert("Import Data", `Cannot import as no ${alertFields} field(s) set.`, ui.ButtonSet.OK);
    return;
  }
  // If there's only one debit amount field need to add a field for Credit Amount
  if (debitHandling.length) importFieldMap.push(creditAmountTransFieldName);
  const importFieldMapObject: object = getColumnarObjectFromKeys(importFieldMap, []);
  const emptyImportObject: object = getColumnarObjectFromKeys(importFieldMap, "");
  // Fetch Import & Transactions sheets and their data
  const shtNameImport: string = "Import";
  const shtNameTransactions: string = "Transactions";
  const shtTransactions: Sheet = ss.getSheetByName(shtNameTransactions);
  const dataImport: any[][] = shtImport.getDataRange().getValues();
  const dataTransactions: any[][] = shtTransactions.getDataRange().getValues();
  const frImport: number = shtImport.getFrozenRows();
  const frTransactions: number = shtTransactions.getFrozenRows();
  const rowIdxFields: number = frImport-1;
  const headerRowTransactions: string[] = ss.getRangeByName("TransactionsHeader").getValues()[0];
  const acctIdList: string[] = getAccountIdList();
  const entityIdList: string[] = getEntityIdList();
  // Fetch new Transactions to insert
  const newTransactions: object = dataImport.reduce((accum: object, row: string[], idx: number): object => {
    if (idx === 0 && openBal) {
      // do the opening balance transaction
      // first header row
      const headerRow = {...emptyImportObject};
      headerRow[dateTransFieldName] = openBalDate;
      headerRow[detailTransFieldName] = "Opening balance of account";
      copyObjectContentsIntoObjectAsRow(accum, headerRow);
      // debit side
      const debitObject = {...emptyImportObject};
      debitObject[dateTransFieldName] = openBalDate;
      debitObject[debitAmountTransFieldName] = Math.abs(openBal);
      debitObject[acctIdTransFieldName] = openBal > 0 ? acctID : openBalAccountID;
      copyObjectContentsIntoObjectAsRow(accum, debitObject);
      // credit side
      const creditObject = {...emptyImportObject};
      creditObject[dateTransFieldName] = openBalDate;
      creditObject[creditAmountTransFieldName] = Math.abs(openBal);
      creditObject[acctIdTransFieldName] = openBal < 0 ? acctID : openBalAccountID;
      copyObjectContentsIntoObjectAsRow(accum, creditObject);
      // check if acctID and openBalAccountID's exist
      if (!~acctIdList.indexOf(acctID))
        errorReport.push(["Import Sheet Error", `Account ID in header area ${acctID} is not a valid account. Please change.`]);
      if (!~acctIdList.indexOf(openBalAccountID))
        errorReport.push(["Import Sheet Error", `Opening Balance Account ID field in header area is not a valid account. Please change`]);
    }
    if (idx >= frImport) {
      // loop through each cell in the row to extract the referenced columns
      const newRow: object = row.reduce((accumRow: object, cell: string, idxCell: number): object => {
        const importFieldHeader: string = dataImport[rowIdxFields][idxCell];
        if (importFieldHeader) accumRow[importFieldHeader] = cell;
        return accumRow;
      }, {...emptyImportObject});
      // check if valid account ID
      if (!~acctIdList.indexOf(newRow[acctIdTransFieldName]))
        errorReport.push(["Import Sheet Error", `Error on row ${idx+1} invalid ${acctIdTransFieldName} ${newRow[acctIdTransFieldName]} does not exist, please change`]);
      // check if valid Entity ID
      if (~importFieldMap.indexOf("Entity") && !~entityIdList.indexOf(newRow["Entity"]))
        errorReport.push(["Import Sheet Error", `Error on row ${idx+1} invalid Entity ${newRow["Entity"]} not found. Please edit row or add new Entity.`]);
      // If the `Debit Amount Handling` cell is set then modify the `Debit Amount` and `Credit Amount` fields.
      if (debitHandling) {
        if (~debitHandling.indexOf("Debit is +")) {
          if (newRow[debitAmountTransFieldName] < 0) {
            // swap the credit amount field over and delete the debit amount field
            newRow[creditAmountTransFieldName] = Math.abs(newRow[debitAmountTransFieldName]);
            delete newRow[debitAmountTransFieldName];
          }
        } else if (~debitHandling.indexOf("Debit is -")) {
          if (newRow[debitAmountTransFieldName] < 0) {
            newRow[debitAmountTransFieldName] = Math.abs(newRow[debitAmountTransFieldName]);
          } else {
            newRow[creditAmountTransFieldName] = newRow[debitAmountTransFieldName];
            delete newRow[debitAmountTransFieldName];
          }
        }
      }
      // Create the Main Line record
      const mainLineRow: object = {...emptyImportObject};
      mainLineRow[dateTransFieldName] = newRow[dateTransFieldName];
      mainLineRow[detailTransFieldName] = newRow.hasOwnProperty(mainLineFieldName) ? newRow[mainLineFieldName] : "";
      copyObjectContentsIntoObjectAsRow(accum, mainLineRow);
      // Create the Debit Row (only shallow copy needed)
      const debitRow: object = {...newRow};
      const creditRow: object = {...newRow};
      if (newRow.hasOwnProperty(debitAmountTransFieldName)) {
        creditRow[acctIdTransFieldName] = debitRow[acctIdTransFieldName];
        creditRow[creditAmountTransFieldName] = debitRow[debitAmountTransFieldName];
        debitRow[acctIdTransFieldName] = acctID;
        debitRow[creditAmountTransFieldName] = "";
        copyObjectContentsIntoObjectAsRow(accum, debitRow);
        creditRow[debitAmountTransFieldName] = "";
        copyObjectContentsIntoObjectAsRow(accum, creditRow);
      } else {
        debitRow[acctIdTransFieldName] = creditRow[acctIdTransFieldName];
        debitRow[debitAmountTransFieldName] = creditRow[creditAmountTransFieldName];
        creditRow[acctIdTransFieldName] = acctID;
        creditRow[debitAmountTransFieldName] = "";
        copyObjectContentsIntoObjectAsRow(accum, creditRow);
        debitRow[creditAmountTransFieldName] = "";
        copyObjectContentsIntoObjectAsRow(accum, debitRow);
      }
    }
    return accum;
  }, importFieldMapObject);
  // Export data to Transactions sheet
  // To keep the formulas in other cells intact, insertion of data will be per column therefore need to
  // find out which row to start. Note: `.getDataRange().getValues()` returns everything in the sheet including
  // empty cells where formulas are.
  const idxLastTransactionRow: number = dataTransactions.reduce((accum: number, row: string[], idx: number): number => {
    // if row contains something skip
    if (row.join('').length) {
      // if we find an empty row in the middle of transactions we want to reset `accum`
      if (idx > accum) accum = dataTransactions.length;
      return accum;
    }
    if (idx >= frTransactions && idx < accum ) accum = idx;
    return accum;
  }, dataTransactions.length);
  // publish results to Transaction sheet - input cells only! And if there are no errors!
  if (errorReport.length === 0) {
    importFieldMap.forEach((key) => {
      const colIdx = headerRowTransactions.indexOf(key);
      const vals: any[][] = newTransactions[key];
      if (key && ~colIdx) {
        shtTransactions.getRange(idxLastTransactionRow+1, colIdx+1, vals.length, 1).setValues(vals);
      }
    });
    const clearImportContents: Button = ui.alert("Import Data", "This import has been successful, do you want to clear the Import data lines?", ui.ButtonSet.YES_NO);
    // clear data if successful import
    if (clearImportContents === ui.Button.YES) {
      shtImport.getRange(frImport, 1, dataImport.length, dataImport[0].length).clearContent();
    }
    // clear error panel
    importResult.setValue("");
  } else {
    // output Error report to Error Sheet
    const shtErrors = ss.getSheetByName("Errors");
    shtErrors.clearContents();
    shtErrors.getRange(1, 1, errorReport.length, errorReport[0].length).setValues(errorReport);
    importResult.setValue("Please view ERRORS sheet for further details");
    ui.alert("Import of data did not happen due to import errors, please review ERRORS sheet.");
  }
}
