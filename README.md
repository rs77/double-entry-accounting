# Double Entry Accounting System Using Google Sheets

These script is behind the Double Entry Accounting Google Sheets System which can help take your personal finances or small side hustle/business to the next level by keeping a register of your accounts in one place.

The spreadsheet itself can be found on the website here:

https://recurrent.com.au/


## Script Details

The script currently contains only one module which assist with importing data from 