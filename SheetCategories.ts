

export const getAccountIdList = () => {
    const ss = SpreadsheetApp.getActiveSpreadsheet();
    const sheetName = "Categories";
    const sht = ss.getSheetByName(sheetName);
    const data = sht.getDataRange().getValues();
    const acctIdFieldName = ss.getRangeByName("setTransactionAccountIDField").getValue();
    const header = ss.getRangeByName("CategoriesHeader").getValues()[0];
    const idxCol = header.reduce((accum: number, cell: any, idx: number): number => {
        if (cell === acctIdFieldName) accum = idx;
        return accum;
    }, -1);
    const fr = sht.getFrozenRows();
    return data.reduce((accum: string[], row: any, idx: number): string[] => {
        if (idx >= fr) {
            const acctId = row[idxCol];
            if (acctId) accum.push(acctId);
        }
        return accum;
    }, []);
}